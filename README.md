# Bitbucket Pipe: bitbucket-upload-file

Upload a file from Bitbucket Pipelines to [Bitbucket Downloads](https://confluence.atlassian.com/bitbucket/deploy-build-artifacts-to-bitbucket-downloads-872124574.html).
Simple use cases include uploading an artifact, tar.gz file or or storing generated test reports etc.


## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/bitbucket-upload-file:0.1.3
  variables:
    BITBUCKET_USERNAME: '<string>'
    BITBUCKET_APP_PASSWORD: '<string>'
    FILENAME: '<string>'
    # ACCOUNT: '<string>' # Optional
    # REPOSITORY: '<string>' # Optional
    # DEBUG: '<boolean>' # Optional
```

## Variables

| Variable                     | Usage                                                       |
| ---------------------        | ----------------------------------------------------------- |
| BITBUCKET_USERNAME (*)       | Bitbucket user that will be used to upload the file. |
| BITBUCKET_APP_PASSWORD (*)   | [Bitbucket app password][Bitbucket app password] of the user that will be used to upload the file. |
| FILENAME (*)                 | Name of the local file to upload. |
| ACCOUNT                      | Name of the Bitbucket account in which the file will be uploaded to. If empty, it will be uploaded to the same account where the pipeline runs. Default: `${BITBUCKET_REPO_OWNER}`. |
| REPOSITORY                   | Name of the Bitbucket repository in which the file will be uploaded to. If empty, it will be uploaded to the same repository where the pipeline runs. Default: `${BITBUCKET_REPO_SLUG}`. |
| DEBUG                        | Flag to turn on extra debug information. Default: `false`. |

_(*) = required variable._


## Prerequisites
To use this pipe, you need to generate an [app password][Bitbucket app password]. Remember to check the `Repositories write` and `Repositories read` permissions when generating the app password. If you want to upload a file to a repository owned by a team account, make sure you have the correct access to the repository.


## Examples

Basic example:

Deploy a file to Bitbucket Downloads for the current repository.

```yaml
script:
  - pipe: atlassian/bitbucket-upload-file:0.1.3
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
      FILENAME: 'package.json'
```

Advanced example:

Deploy a file to Bitbucket Downloads for the other repository in the other account where user has access.

```yaml
script:
  - pipe: atlassian/bitbucket-upload-file:0.1.3
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
      FILENAME: 'package.json'
      ACCOUNT: 'team-account'
      REPOSITORY: 'awesome-repository-name'
```

## Support

If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,downloads,file
[variables-in-pipelines]: https://confluence.atlassian.com/bitbucket/variables-in-pipelines-794502608.html
[Bitbucket app password]: https://confluence.atlassian.com/bitbucket/app-passwords-828781300.html