import os

import requests
from requests.auth import HTTPBasicAuth

from bitbucket_pipes_toolkit.test import PipeTestCase


BITBUCKET_BASE_URL = "https://bitbucket.org"
BITBUCKET_API_BASE_URL = "https://api.bitbucket.org/2.0"


def delete_file_from_downloads(account, token, repository, filename):
    url = f"{BITBUCKET_API_BASE_URL}/repositories/{account}/{repository}/downloads/{filename}"
    auth = HTTPBasicAuth(account, token)

    return requests.delete(url, auth=auth)


class UploadArtifactTestCase(PipeTestCase):
    filename = 'pipe.yml'

    def test_fail_no_params(self):
        result = self.run_container()

        self.assertRegex(
            result, rf"✖ Validation errors")

    def test_upload_basic_success(self):
        account = os.getenv('BITBUCKET_REPO_OWNER')
        repository = os.getenv('BITBUCKET_REPO_SLUG', 'test-bitbucket-upload-artifact')

        result = self.run_container(
            environment={
                # pipelines env variables
                'BITBUCKET_REPO_OWNER': os.getenv('BITBUCKET_REPO_OWNER'),
                'BITBUCKET_REPO_SLUG':  os.getenv('BITBUCKET_REPO_SLUG'),
                # pipe variables
                'BITBUCKET_USERNAME': os.getenv('BITBUCKET_USERNAME'),
                'BITBUCKET_APP_PASSWORD': os.getenv('BITBUCKET_APP_PASSWORD'),
                'FILENAME': self.filename,
            }
        )

        delete_file_from_downloads(
            os.getenv('BITBUCKET_USERNAME'),
            os.getenv('BITBUCKET_APP_PASSWORD'),
            repository,
            self.filename,
        )
        self.assertRegex(
            result, rf"✔ Successfully uploaded file {self.filename} to {BITBUCKET_BASE_URL}/{account}/{repository}/downloads")

    def test_upload_other_account_success(self):
        account = 'bitbucketpipelines'
        repository = 'example-bitbucket-upload-file'

        result = self.run_container(
            environment={
                # pipelines env variables
                'BITBUCKET_REPO_OWNER': os.getenv('BITBUCKET_REPO_OWNER'),
                'BITBUCKET_REPO_SLUG': os.getenv('BITBUCKET_REPO_SLUG'),
                # pipe variables
                'BITBUCKET_USERNAME': os.getenv('BITBUCKET_USERNAME'),
                'BITBUCKET_APP_PASSWORD': os.getenv('BITBUCKET_APP_PASSWORD'),
                'ACCOUNT': account,
                'REPOSITORY': repository,
                'FILENAME': self.filename,
            }
        )

        delete_file_from_downloads(
            account,
            os.getenv('BITBUCKET_APP_PASSWORD'),
            repository,
            self.filename,
        )
        self.assertRegex(
            result, rf"✔ Successfully uploaded file {self.filename} to {BITBUCKET_BASE_URL}/{account}/{repository}/downloads")

    def test_fail_if_file_not_exist(self):
        filename = 'file-not-exist'

        result = self.run_container(
            environment={
                'BITBUCKET_REPO_OWNER': os.getenv('BITBUCKET_USERNAME'),
                'BITBUCKET_USERNAME': os.getenv('BITBUCKET_USERNAME'),
                'BITBUCKET_APP_PASSWORD': os.getenv('BITBUCKET_APP_PASSWORD'),
                'REPOSITORY': os.getenv('REPOSITORY', 'test-bitbucket-upload-artifact'),
                'FILENAME': filename,
            }
        )

        self.assertRegex(
            result, rf"✖ File {filename} doesn't exist")

    def test_fail_if_wrong_repository(self):
        result = self.run_container(
            environment={
                'BITBUCKET_REPO_OWNER': os.getenv('BITBUCKET_USERNAME'),
                'BITBUCKET_USERNAME': os.getenv('BITBUCKET_USERNAME'),
                'BITBUCKET_APP_PASSWORD': os.getenv('BITBUCKET_APP_PASSWORD'),
                'REPOSITORY': 'not-exist-wrong-repo',
                'FILENAME': self.filename,
            }
        )

        self.assertIn(f"Repository {os.getenv('BITBUCKET_USERNAME')}/not-exist-wrong-repo not found", result)

    def test_fail_if_wrong_account(self):
        result = self.run_container(
            environment={
                'BITBUCKET_REPO_OWNER': os.getenv('BITBUCKET_USERNAME'),
                'BITBUCKET_USERNAME': 'not-exist-wrong-account',
                'BITBUCKET_APP_PASSWORD': os.getenv('BITBUCKET_APP_PASSWORD'),
                'REPOSITORY': os.getenv('REPOSITORY', 'test-bitbucket-upload-artifact'),
                'FILENAME': self.filename,
            }
        )

        self.assertRegex(
            result, rf"✖ API request failed with status 401")

    def test_fail_if_wrong_bitbucket_app_password(self):
        result = self.run_container(
            environment={
                'BITBUCKET_REPO_OWNER': os.getenv('BITBUCKET_USERNAME'),
                'BITBUCKET_USERNAME': os.getenv('BITBUCKET_USERNAME'),
                'BITBUCKET_APP_PASSWORD': 'wrong-app-password',
                'REPOSITORY': os.getenv('REPOSITORY', 'test-bitbucket-upload-artifact'),
                'FILENAME': self.filename,
            }
        )

        self.assertRegex(
            result, rf'✖ API request failed with status 401')
