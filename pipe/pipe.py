import os
import json
from pathlib import Path

import yaml
import requests
from requests.auth import HTTPBasicAuth

from bitbucket_pipes_toolkit import Pipe, get_logger

BITBUCKET_BASE_URL = "https://bitbucket.org"
BITBUCKET_API_BASE_URL = "https://api.bitbucket.org/2.0"


logger = get_logger()

schema = {
    'BITBUCKET_USERNAME': {'type': 'string', 'required': True},
    'BITBUCKET_APP_PASSWORD': {'type': 'string', 'required': True},
    'FILENAME': {'type': 'string', 'required': True},
    'ACCOUNT': {'type': 'string', 'required': False, 'nullable': True, 'default': os.getenv('BITBUCKET_REPO_OWNER')},
    'REPOSITORY': {'type': 'string', 'required': False, 'nullable': True, 'default': os.getenv('BITBUCKET_REPO_SLUG')},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}


class UploadFilePipe(Pipe):

    def run(self):
        super().run()

        logger.info('Executing the pipe...')
        username = self.get_variable('BITBUCKET_USERNAME')
        app_password = self.get_variable('BITBUCKET_APP_PASSWORD')
        filename = self.get_variable('FILENAME')
        account = self.get_variable('ACCOUNT')
        repository = self.get_variable('REPOSITORY')

        if not Path(filename).is_file():
            self.fail(f"File {filename} doesn't exist.")

        url = f"{BITBUCKET_API_BASE_URL}/repositories/{account}/{repository}/downloads"
        auth = HTTPBasicAuth(username, app_password)

        with open(filename, 'rb') as f:
            logger.info(f"Start uploading file {filename}...")
            response = requests.post(url, auth=auth, files={'files': (filename, f)})

        # Bitbucket API returns status code 201 'Created' after success upload
        # https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/downloads#post
        if response.status_code == 201:
            link = f"{BITBUCKET_BASE_URL}/{account}/{repository}/downloads"
            basename = os.path.basename(filename)

            self.success(f"Successfully uploaded file {filename} to {link}")
            logger.info(f"You can download the file by clicking in this link: {link}/{basename}")
        elif response.status_code == 401:
            # clarifying message for users
            self.fail(
                f"API request failed with status 401. Check your username and app password and try again.")
        else:
            try:
                data = response.json()
            except json.decoder.JSONDecodeError:
                data = None

            message = data['error']['message'] if data and data.get('error') else response.text

            self.fail(
                f"Failed to upload file {filename}. Status code: {response.status_code}. "
                f"Message: {message}."
            )


if __name__ == '__main__':
    metadata = yaml.safe_load(open('/pipe.yml', 'r'))
    pipe = UploadFilePipe(schema=schema, pipe_metadata=metadata, check_for_newer_version=True)
    pipe.run()
